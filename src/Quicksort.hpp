#ifndef __QUICKSORT_H__
#define __QUICKSORT_H__

#include <vector>

template <typename T>
class QuickSort
{
public:
    //dont change this constructor
    QuickSort(){};
    std::vector<T> solution(std::vector<T>);
    ~QuickSort(){};
};


#endif