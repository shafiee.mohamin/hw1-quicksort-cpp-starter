#include <iostream>
#include <vector>
#include "Quicksort.hpp"
#include "Quicksort.cpp"
using namespace std;

template<typename T>
class QuickSort;

int main()
{
    int n ;
    cout << "Enter number of numbers" << endl ;
    cin >> n ;

    vector<int> input;

    for(int i = 0 ; i < n ; i++){
        int x ;
        cin >> x, input.push_back(x);
    }

    QuickSort<int> quicksort ;

    vector<int> result = quicksort.solution(input);

    for(auto item : result)
        cout << item << " ";

    return 0;
}