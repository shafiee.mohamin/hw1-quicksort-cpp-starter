#/bin/bash

make_src_result=`make --directory="./src"`
make_test_result=`make --directory="./test"`

echo "***********************************"
echo "make result"
echo "make src_result: $make_src_result"
echo "make test_result: $make_test_result"
echo "***********************************"

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

result=`make run --directory="./test"`
res_arr=($result)
echo "${result}"
num=(${res_arr[5]})
echo "${num[0]}"
echo $1
echo "***********************************"
echo "build status"
if [ ${res_arr[1]} -ne $1 ]
then
    echo "${RED}"
    echo "Build Failed"
    echo "total tests: ${1}"
    echo "failed tests: ${res_arr[1]}"
    echo "${NC}"
else 
    echo "${GREEN}"
    echo "Build Success"
    echo "${NC}"
fi

echo "***********************************"