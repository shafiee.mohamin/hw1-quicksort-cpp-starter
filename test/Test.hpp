#ifndef __TEST_H__
#define __TEST_H__
#include <vector>

namespace Tests{

    class Test
    {
    private:
        std::vector<std::vector <int> > input, output;
        static int counter;
    public:
        Test(){};
        void setup();
        int test1();
        int test2();
        int test3();
        int test4();
        void run();
        static int getCounter();
        ~Test(){};
    };

}

#endif