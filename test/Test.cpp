#include "Test.hpp"
#include "../src/Quicksort.hpp"
#include "../src/Quicksort.cpp"
#include <vector>
#include <iostream>
#include <fstream>
#include <ctype.h>
using namespace std;
using namespace Tests;

template<typename T>
class QuickSort;

int Test::counter = 0;

void Test::setup()
{
    try{
        string line ;
        ifstream myfile ("input.txt") ;
        if (myfile.is_open())
        {
            while ( getline (myfile,line) )
            {
                vector<int> hold ;
                for(int i = 0 ; i < line.size() ; i++)
                    if(isdigit(line.at(i)))
                        hold.push_back(atoi(&line.at(i)));

                this->input.push_back(hold);
            }
            myfile.close();
        }

        ifstream outputfile ("output.txt");
        if (outputfile.is_open())
        {
            while ( getline (outputfile,line) )
            {
                vector<int> hold ;
                for(int i = 0 ; i < line.size() ; i++)
                    if(isdigit(line.at(i)))
                        hold.push_back(atoi(&line.at(i)));

                this->output.push_back(hold);
            }
            outputfile.close();
        }

    }catch(const char *msg){
        cout << msg ;
    }

}

int Test::test1()
{
    QuickSort<int> q ;

    vector<int> result = q.solution(this->input.at(0));
    if(result.size() != this->output.at(0).size()) return 0;

    for(int i = 0 ; i < input.size(); i++){
        if(output.at(0).at(i) != result.at(i))
            return 0;
    }

    Test::counter++ ;
    return 1;
}

int Test::test2()
{
    QuickSort<int> q ;
    vector<int> result = q.solution(this->input.at(1));

    if(result.size() != this->output.at(1).size()) return 0;

    for(int i = 0 ; i < input.size(); i++){
        if(output.at(1).at(i) != result.at(i))
            return 0;
    }

    Test::counter++ ;
    return 1;
}


int Test::test3()
{
    QuickSort<int> q ;

    vector<int> result = q.solution(this->input.at(2));

    if(result.size() != this->output.at(2).size()) return 0;

    for(int i = 0 ; i < input.size(); i++){
        if(output.at(2).at(i) != result.at(i))
            return 0;
    }

    Test::counter++ ;
    return 1;
}

int Test::test4()
{
    QuickSort<int> q ;

    vector<int> result = q.solution(this->input.at(3));

    if(result.size() != this->output.at(3).size()) return 0;

    for(int i = 0 ; i < input.size(); i++){
        if(output.at(3).at(i) != result.at(i))
            return 0;
    }

    Test::counter++ ;
    return 1;
}

int Test::getCounter()
{
    return Test::counter ;
}

void Test::run()
{
    this->test1();
    this->test2();
    this->test3();
    this->test4();
}