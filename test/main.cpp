#include <iostream>
#include "Test.hpp"
#include <unistd.h>
using namespace std;

class Test;

int main(){
    try {
        Tests::Test test;
        test.setup();
        test.run();
        cout << Tests::Test::getCounter();
        return Tests::Test::getCounter();
    }catch(std::out_of_range msg){
        cout << "0" ;
        return 0;
    }
}